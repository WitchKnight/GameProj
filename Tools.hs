module Tools where

import Types
import Preload

import Debug.Trace
import Control.Lens
import Graphics.Gloss
import qualified Data.HashMap.Lazy as DHL
import Linear.V2
gameExit :: Game -> Game
gameExit game = game & flags.exit .~ True

darken :: Int -> Color -> Color
darken 1 col = col
darken n col = dark (darken (n-1) col) 

getLevelPicture :: Game -> Picture
getLevelPicture game = game^.status.cMap


makeLevelPicture :: Game-> Game
makeLevelPicture game = game & status.cMap .~ finalresult
  where
    cLmap = DHL.toList (game^.status.cLevel.lvlmap)
    whatSet = game^.status.cLevel.tileset
    (twidth, theight) = (game^.config.tilesize._1, game^.config.tilesize._2)
    finalresult = trace "finalresult" $  pictures [translate (fromIntegral $ twidth*xpos) (fromIntegral $ theight*ypos) (getTile whatSet tile) | ((V2 xpos ypos),tile) <- cLmap]



queueEvents :: [GameEvent] -> Game -> Game
queueEvents newevents game = game & events .~ (game^.events ++ newevents)



--check if the Map is whole (contains all cases in the square of  size mapSize)
checkMap :: GameLevel -> Bool
checkMap gLevel = trace (show allcases) $ checkMap' allcases
  where
    (V2 mx my) = gLevel^.mapSize
    map = gLevel^.lvlmap
    allcases = [(a,b) | a <- [1..(mx-1)], b <- [1..(my-1)]]
    checkMap' [] = True
    checkMap' ((x,y):ns)
      | DHL.lookup (V2 x y) map /= Nothing = checkMap' ns
      | otherwise = trace (show (x,y)) $ False && (checkMap' ns) 


updateCamera :: Game -> Game
updateCamera game = game  & status.camera .~ (game^.status.camera + game^.status.cameraDirection)

unMaybe:: Maybe a -> a
unMaybe thing = thething
  where 
    (Just thething) = thing
    
makeStatic :: Picture -> Picture
makeStatic pic = case pic of
  Bitmap m n bmpd False -> Bitmap m n bmpd True
  _                     -> pic

trigPic :: Picture -> Picture -> Picture
trigPic pic pic'
  | pic' == pic = blank
  | otherwise = pic


applyN ::Int  -> (a -> a) -> a -> a
applyN 0 f a =  a
applyN n f a = f $ applyN (n-1) f a
