{-# LANGUAGE TemplateHaskell, OverloadedStrings, DeriveGeneric #-}
module LoadExternal where
import Types
import Tools
import Actors

import Data.Text
import Data.Aeson
import Control.Lens
import GHC.Generics
import Data.List.Split
import System.Environment
import qualified Data.Map.Lazy  as M
import qualified Data.HashMap.Lazy as DHL
import qualified Data.ByteString.Lazy as B
import System.IO.Unsafe
import Linear.V2
import Debug.Trace

getRaw :: String -> B.ByteString
getRaw whichlevel = unsafePerformIO $ B.readFile $ "Levels/" ++ whichlevel ++ ".json"




getLevel :: String -> GameLevel
getLevel whichlevel 
  | raw == emptyRawLevel = emptyLevel
  | otherwise = gamelevel
  where
    raw = convert $ decode $ getRaw whichlevel
    gamelevel= GameLevel
      { _lvlmap         = makeMap (V2 0 0) $ raw^.jMap 
      , _actors         = M.fromList mapactors
      , _seenMap        = DHL.empty
      , _oldSeenMap     = DHL.empty
      , _tileset        = read (raw^.jTileset) :: Tileset
      , _mapSize        = V2 mpx mpy
      , _lname          = raw^.jName
      , _djikstra       = DHL.empty
      , _startingSquare = extSquare $ raw^.jStartingSquare
      }
    (mpx:mpy:[]) = raw^.jmapSize
    makeMap :: V2 Int -> [Int] -> LevelMap-- counter -> list -> result
    makeMap _ [] = DHL.empty
    makeMap (V2 x y ) (n:ns)
      | x == (mpx-1) = if y == (mpy-1) then DHL.insert (V2 x y) n DHL.empty else DHL.insert (V2 x y) n (makeMap (V2 0 (y+1)) ns)
      | otherwise = DHL.insert (V2 x y) n (makeMap (V2 (x+1) y) ns)
    extSquare (x:y:x':y':[]) = V2 (V2 x y) (V2 x' y')
    mapactors = makeMapActors 10 $ raw^.jActors
    makeMapActors :: Int -> [Int] -> [(Int,Actor)]
    makeMapActors _ [] = []
    makeMapActors n (z:x:y:xs) = (n, newActor) : (makeMapActors (n+1) xs)
      where
        newActor = (loadMapActor z) & acoord .~ (V2 x y)
        
convert :: Maybe RawLevel -> RawLevel
convert maybelevel =
  case maybelevel of 
    Nothing -> trace "error loading level" $ emptyRawLevel 
    Just thelevel -> thelevel



--  ~ getItems ;; String -> HashMap Item 
--  ~ getItems which = 





