module Preload where
import Graphics.Gloss
import Graphics.Gloss.Juicy
import Graphics.Gloss.Game
import Debug.Trace

import Types
tilefolder  = "Images/Old/Tiles/"
actorfolder = "Images/Old/Actors/"
iconfolder  = "Images/Old/Icons/"
menufolder  = "Images/Old/Menus/"


getTile :: Tileset -> Int ->  Picture
getTile whatSet x = trace ("tile" ++ (show x)) $! genericTiles !! x


genericTiles =             [ png (tilefolder ++ "wall1_cave.png") 
                           , png (tilefolder ++ "wall2_cave.png") 
                           , png (tilefolder ++ "wall3_cave.png") 
                           , png (tilefolder ++ "wallCH_cave.png")
                           , png (tilefolder ++ "wallCV_cave.png")
                           , png (tilefolder ++ "doorOH.png")     
                           , png (tilefolder ++ "doorOV.png")     
                           , png (tilefolder ++ "doorCV.png")     
                           , png (tilefolder ++ "doorCH.png")     
                           , png (tilefolder ++ "floor.png")
                           , blank]                              


preloadedActorSprites :: [Picture]
preloadedActorSprites =     [ png (actorfolder ++ "Hero.png")
                            , png (actorfolder ++ "Mage.png")
                            , png (actorfolder ++ "Fighter.png")
                            , png (actorfolder ++ "Archer.png")
                            , scale (23/32) (23/32) $ png ("Images/New/Actors/salamander.png")
                            , png (actorfolder ++ "Adult Salamander.png")
                            , png (actorfolder ++ "Ogre.png")
                            , png (actorfolder ++ "Sprite.png")
                            , png (actorfolder ++ "BossGraboulox")]

fetchActorpic :: String -> Picture
fetchActorpic x
  | x == "Hero"              = preloadedActorSprites !! 0
  | x == "Mage"              = preloadedActorSprites !! 1
  | x == "Swordman"           = preloadedActorSprites !! 2
  | x == "Bowman"            = preloadedActorSprites !! 3
  | x == "Salamander"        = preloadedActorSprites !! 4
  | x == "Adult Salamander"  = preloadedActorSprites !! 5
  | x == "Ogre"              = preloadedActorSprites !! 6
  | x == "Sprite"            = preloadedActorSprites !! 7
  | x == "Mad Golem"         = preloadedActorSprites !! 8
