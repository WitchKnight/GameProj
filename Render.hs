module Render where

import Types
import Preload
import Tools
import LoadExternal
import TextRendering

import qualified Data.Map.Lazy as M
import Linear.V2
import Graphics.Gloss
import Graphics.Gloss.Game
import Control.Lens
import GHC.Word
-- text colors --
tRed = (255,0,0,255)
tBlack = (0,0,0,255)
tWhite = (255,255,255,255)
setAlpha :: (Word8,Word8,Word8,Word8) -> Word8 ->(Word8, Word8,Word8,Word8)
setAlpha (r,g,b,a) a' = (r,g,b,a') 

renderIO :: Game -> IO Picture
renderIO game = do 
              return (render game)
              
render :: Game -> Picture
render game = pictures [picLevel, picActors, picLoading, picUI, picText, picDebug]
  where
    --game config--
    (gw,gh) = (fromIntegral $ game^.config.dimensions._1, fromIntegral $ game^.config.dimensions._2)
    (tsx,tsy) = (fromIntegral $ game^.config.tilesize._1, fromIntegral $ game^.config.tilesize._2)
    gScene = game^.status.scene
    gStage = game^.status.stage
    (V2 cx cy) = game^.status.camera
    (V2 mx my) = game^.status.cLevel.mapSize
    (mx',my') = (fromIntegral mx, fromIntegral my)
    -- GAME TEXT --
    picText 
      | gScene == InGame = pictures [currentItemBox]
      | gScene == Menu = pictures []
      | otherwise = blank
    -- currentItem -- used for item description, and reading currentItems
    bName = translate 0 175 $ scale 1.1 1.1 $ renderDescription (game^.gText.currentItem._2) MainFont 30 (setAlpha tWhite 0 ) tWhite
    bTG = translate (0) 175 $ color white $ rectangleWire 1020 33 
      where
        blength = fromIntegral $ length (game^.gText.currentItem._2)
    bBG = color white $ rectangleWire 1020 300 
    bText =  translate 0 130 $ scale 1.1 1.1 $  game^.preloadedText.currentDesc
    currentItemBox
      | (gScene == Menu ) && (game^.flags.itemDesc == True) = translate 0 (1- (gh/2- 150)) $ scale 0.9 0.9 $  pictures [bBG,bText,bTG,bName]
      | otherwise = blank
    --input --
    -- USER INTERFACE --
    picUI 
      | gScene == Introduction =  pictures [introMessage', playerInput]
      | otherwise = blank
    introMessage' = translate (300) 0 $ game^.preloadedText.introMessage
    playerInput = translate (300) (-40) $ renderIntro (game^.status.pInput) MainFont 30 (setAlpha tWhite 0 ) tWhite
    --  LEVEL DISPLAY --
    picLevel
      | gScene == InGame = adjustToMap $  getLevelPicture game -- see Tools
      | otherwise = blank
    -- LOADING --
    picLoading 
      | gScene == Loading = translate 300 0 $ game^.preloadedText.loading
      | otherwise = blank
    -- MENU -- 
    picMenu
      | gScene == Menu = pictures []
      | otherwise = blank
    -- Actors --
    picActors 
      | gScene == InGame = adjustToMap $ makeActorsPicture game
      | otherwise = blank
    --DEBUG--
    picDebug = translate (-gw/3) (gh/3) $  color white $ pictures [picScene, picStage]
    picScene = scale 0.2 0.2 $ text $ show $ gScene
    picStage = scale 0.2 0.2 $ translate (-100) 0 $ text $ show $ gStage
    
    -- specific render functions
    adjustToMap  pic = scale 2 2  $ translate (cx*2 - 0.5*fromIntegral (mx*tsx)) (cy*2 - 0.5*fromIntegral(my*tsy)) $ pic
-- global render functions
   
    
   
makeMosaic :: Picture -> V2 Float ->  V2 Float  -> V2 Float-> Float -> Float  -> Picture
makeMosaic picture picDimensions fromwhere step lines columns  =  translate fx fy $ pictures [ translate  x  y picture | x <- xcases columns, y <- ycases lines ]
      where
        xcases n
          | n == 0 = []
          | otherwise = (dx*(n-1)+sx*(n-1)):(xcases (n-1)) 
        ycases n
          | n == 0 = []
          | otherwise = (dy*(n-1)+sy*(n-1)):(ycases (n-1))
        V2 fx fy = fromwhere
        V2 dx dy = picDimensions
        V2 sx sy = V2 0 0
    
makeMosaicf ::  V2 Float -> V2 Float -> Float -> Float -> (Float -> Float -> Picture) -> V2 Float -> Picture
makeMosaicf fromwhere step lines columns picFunc args = makeMosaic (picFunc x y) args fromwhere step lines columns
   where
    V2 x y = args
   
   
-- | returns a Picture containing all actors sprites at their locations.
makeActorsPicture :: Game -> Picture
makeActorsPicture game  = pictures (toPictures actors'')
    where
      (twidth,theight) = game^.config.tilesize
      actors' = game^.status.cLevel.actors
      actors'' = M.toList actors'
      toPictures :: [(Int,Actor)] -> [Picture]
      toPictures [] = []
      toPictures (x:xs) = (translate (fromIntegral $ ax*twidth) (fromIntegral $ ay*(theight)) (fetchActorpic $ show (x^._2.aclass.actorsprite) )): toPictures xs
        where
          V2 ax ay  = x^._2.acoord
   
