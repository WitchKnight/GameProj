module GameSound where

import Types
import Tools
import Control.Monad (when, unless)
import Control.Concurrent
import Debug.Trace
import System.IO.Unsafe
import Data.List
import Control.Lens
import Control.Monad (void)
import System.Exit (exitFailure)
import System.IO ( hPutStrLn, stderr )
import qualified Graphics.UI.SDL.Mixer.General as SDL.Mixer
import qualified Graphics.UI.SDL.Mixer.Channels as SDL.Mixer.Channels
import qualified Graphics.UI.SDL.Mixer.Music as SDL.Mixer.Music
import qualified Graphics.UI.SDL.Mixer.Types as SDL.Mixer.Types
import qualified Graphics.UI.SDL.Mixer.Samples as SDL.Mixer.Samples

initAudio :: IO ()
initAudio = void $ do
  _result <- SDL.Mixer.openAudio 44100 SDL.Mixer.AudioS16LSB 2 4096
  SDL.Mixer.Channels.allocateChannels 16

loadMusic :: String -> IO (Maybe Music)
loadMusic fp = fmap (fmap (Music fp)) $ SDL.Mixer.Music.tryLoadMUS fp

playMusic :: Music -> IO ()
playMusic m = do
  SDL.Mixer.Music.setMusicVolume 100
  SDL.Mixer.Music.playMusic (unMusic m) (-1)

-- | Stop playing music
stopMusic :: IO ()
stopMusic = SDL.Mixer.Music.haltMusic

-- | Is music playing?
musicPlaying :: IO Bool
musicPlaying = SDL.Mixer.Music.playingMusic

-- | Load an audio file.
loadAudio :: String -> IO (Maybe Audio)
loadAudio fp = fmap (fmap (Audio fp)) $ SDL.Mixer.Samples.tryLoadWAV fp

-- | Play an audio file for the given number of seconds.
-- This function spawns a new OS thread. Remember to compile your program
-- with the threaded RTS.
playFile :: Audio -> Int -> IO ()
playFile wav t = void $ forkOS $ do 
  _v <- SDL.Mixer.Channels.playChannel (-1) (unAudio wav) 0
  threadDelay (t * 1000)

playSoundEffect :: SoundEffect -> IO ()
playSoundEffect effect = do
    aud <- trace ("playing " ++ (show effect) ++".wav")  $ loadAudio $ "/home/xime/Documents/Projets/FightingSpirit/Sound/"++ (show effect) ++ ".wav"
    case aud of 
      Just aud' ->    playFile aud' 1
      Nothing -> trace ("Unable to load file") $     return()
    
    
    
