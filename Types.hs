{-# LANGUAGE TemplateHaskell, OverloadedStrings, DeriveGeneric #-}
module Types where

import Data.Aeson
import qualified Data.Map.Lazy as M
import Data.HashMap.Lazy
import Control.Lens
import qualified Data.Vector as V
import Linear.V2
import GHC.Generics
import Graphics.Gloss
import qualified Data.ByteString.Lazy as B
import qualified Graphics.UI.SDL.Mixer.General as SDL.Mixer
import qualified Graphics.UI.SDL.Mixer.Channels as SDL.Mixer.Channels
import qualified Graphics.UI.SDL.Mixer.Music as SDL.Mixer.Music
import qualified Graphics.UI.SDL.Mixer.Types as SDL.Mixer.Types
import qualified Graphics.UI.SDL.Mixer.Samples as SDL.Mixer.Samples

-- GAME TYPES
data Tileset = Cave | Temple deriving (Show,Eq,Read)
type Coord = V2 Int
type MapPortion = HashMap Coord Int -- as it says, contains only a portion of the map.
type LevelMap   = HashMap Coord Int -- contains the whole map.

data GameScene = InGame | Introduction | Loading | Menu deriving (Show, Eq, Read)
data GameFont = MainFont | OtherFont deriving (Show,Eq,Read)


data Game = Game
  { _status           :: Status
  , _player           :: Player
  , _events           :: [GameEvent]
  , _flags            :: Flags
  , _preloadedText    :: PreloadedText
  --  ~ , _gAudio           :: GameAudio
  , _gText            :: GameText
  , _config           :: Config
  }deriving (Show, Eq)

data Status = Status
  { _scene            :: GameScene
  , _battleStatus     :: BattleStatus
  , _stage            :: Int
  , _cLevel           :: GameLevel
  , _cMap             :: Picture
  , _camera           :: V2 Float
  , _cameraDirection  :: V2 Float
  , _pInput           :: String
  }deriving (Show, Eq)


data BattleStatus = BattleStatus
  { _cTurn        :: Int
  , _turnCounter  :: Int
  }deriving (Show, Eq)
  
baseBattle = BattleStatus
  { _cTurn    = 0
  , _turnCounter =0
  }
data GameText = GameText
  { _currentItem   :: (String,String)
  , _dialog :: String
  , _combat :: String
  , _input  :: String 
  }deriving (Show, Eq)


data Player = Player
  { _party            :: M.Map Int Actor
  } deriving (Show,Eq)

data PreloadedText = PreloadedText
 { _introMessage    :: Picture
 , _loading         :: Picture
 , _currentDesc     :: Picture
 }deriving (Show, Eq)

data Config = Config
  { _dimensions   ::  (Int,Int)
  , _tilesize     ::  (Int,Int)
  }deriving (Show, Eq)

data GameEvent= NewScene GameScene | NewLevel String deriving (Show,Eq,Read)

  
data Flags = Flags  
  { _exit           :: Bool
  , _itemDesc       :: Bool
  , _initialized    :: Bool  
  }deriving (Show, Eq, Read)



-- ACTOR TYPES --


data Actor = Actor
  { _name             :: String
  , _acoord           :: Coord
  , _aclass           :: ActorClass
  , _astats           :: ActorStats
  --  ~ , _inventory        :: Inventory
  --  ~ , _abilities        :: Abilities
  --  ~ , _statusEffects    :: StatusEffects
  } deriving (Show,Eq)
  
data ActorClass = ActorClass  
  { _vocations        :: HashMap Int Vocation
  , _actorsprite     :: ActorSprite
  --  ~ , _skillSet       :: SkillSet
  } deriving (Show,Eq)

data ActorSprite = Swordman | Bowman | Salamander  deriving (Show,Eq,Read)


data Vocation = Fencer | Archer deriving (Show,Eq,Read)

data ActorStats = ActorStats
  { _actionStats  :: ActionStats
  , _expStats     :: ExpStats
  , _SpiritStats  :: SpiritStats
  , _BodyStats    :: BodyStats
  --  ~ , _mastery      :: Mastery
  } deriving (Show,Eq)
  
data ActionStats = ActionStats
  { _fatigue    :: V2 Int
  , _initiative :: V2 Int
  , _reflexes   :: Int
  , _actState   :: (ActionState, Int)
  } deriving (Show,Eq)

data ActionState = Ready | Waiting deriving (Show,Eq,Read)

data ExpStats = ExpStats
  { _actorLevel  :: Int
  , _experience   :: V2 Int
  } deriving (Show,Eq)

data BodyStats  = BodyStats
  { _constitution :: Int
  , _strength     :: Int
  , _tolerance    :: Int
  , _agility      :: Int
  , _precision    :: Int
  } deriving (Show,Eq)

data SpiritStats = SpiritStats 
  { _power        :: Int
  , _passion      :: Int
  , _grounding    :: Int
  , _awareness    :: Int
  , _insight      :: Int
  , _resilience   :: Int
  , _luck         :: Int
  } deriving (Show,Eq)
-- MAP TYPES


data GameLevel = GameLevel
    { _lvlmap         :: LevelMap
    , _actors         :: M.Map Int Actor
    , _seenMap        :: MapPortion
    , _oldSeenMap     :: MapPortion
    , _tileset        :: Tileset
    , _mapSize        :: Coord
    , _lname          :: String
    , _djikstra       :: LevelMap
    , _startingSquare :: V2 (V2 Int)
    }deriving (Show,Eq)
  
emptyLevel = GameLevel
  { _lvlmap       = empty
  , _actors       = M.empty
  , _seenMap      = empty
  , _oldSeenMap   = empty
  , _tileset      = Cave
  , _mapSize      = V2 0 0 
  , _lname        = "NoLevel"
  , _djikstra     = empty
  , _startingSquare = V2 (V2 0 0) (V2 0 0)
  }
  

-- AUDIO --

data SoundEffect = Confirm | Cancel | Step | Magic | Attack deriving (Show, Eq, Read)


-- JSON --

data RawLevel = RawLevel
  { _jTileset        :: String
  , _jName           :: String
  , _jActors         :: [Int]
  , _jStartingSquare    :: [Int]
  , _jmapSize        :: [Int]
  , _jMap            :: [Int]
  } deriving (Show,Eq,Generic)


emptyRawLevel = RawLevel
  { _jTileset       = "Cave"
  , _jName          = "NoLevel"
  , _jActors        = []
  , _jStartingSquare  = []
  , _jmapSize       = [] 
  , _jMap           = []
  }

data RawItem = RawItem
  { _jitemType       :: String
  , _jitemName       :: String
  , _jitemProps      :: [(String, Int)]
  , _jitemDescr      :: String
  } deriving (Show,Eq,Generic)

emptyRawItem = RawItem
  { _jitemType      = "NoItem"
  , _jitemName      = ""
  , _jitemProps     = []
  , _jitemDescr     = "Empty Item"
  }

instance FromJSON RawLevel
instance ToJSON RawLevel

instance FromJSON RawItem
instance ToJSON RawItem

makeLenses ''RawLevel
makeLenses ''BattleStatus
makeLenses ''ActionStats
makeLenses ''ExpStats
makeLenses ''BodyStats
makeLenses ''Actor
makeLenses ''ActorClass
makeLenses ''Player
makeLenses ''Status
makeLenses ''Game
makeLenses ''Flags
makeLenses ''GameText
makeLenses ''Config
makeLenses ''GameLevel
makeLenses ''PreloadedText
--  ~ makeLenses ''GameAudio
