module TextRendering where

import Data.List.Split
import Types
import Debug.Trace
import System.IO.Unsafe
import Codec.Picture(PixelRGBA8( .. ))
import Graphics.Rasterific
import Graphics.Rasterific.Texture
import Graphics.Gloss.Juicy
import Graphics.Gloss
import GHC.Word
import Graphics.Text.TrueType( loadFontFile )

--item description--
renderDescription :: String -> GameFont  -> Float -> (Word8,Word8,Word8,Word8) -> (Word8,Word8,Word8,Word8) -> Picture
renderDescription text gamefont size bColor tColor = displayChunks ( cutText (60) text  ) gamefont size bColor tColor   
  where
    size' = truncate size
    
    
-- Introduction

renderIntro :: String -> GameFont  -> Float -> (Word8,Word8,Word8,Word8) -> (Word8,Word8,Word8,Word8) -> Picture
renderIntro text gamefont size bColor tColor = displayChunks ( cutText (30) text  ) gamefont size bColor tColor
  where
    size' = truncate size

-- General

displayChunk :: String -> GameFont  -> Float -> (Word8,Word8,Word8,Word8) -> (Word8,Word8,Word8,Word8) -> Picture
displayChunk text gamefont size bColor tColor = 
  case fontErr of
    Left err -> trace err blank
    Right font -> fromImageRGBA8 $ renderDrawing (size'*10*3) (size'*2) (PixelRGBA8 r g b a). withTexture (uniformTexture $ PixelRGBA8 r' g' b' a') $ printTextAt font (PointSize size) (V2 0 35) text
  where
    size' = truncate size
    (r,g,b,a) = bColor
    (r',g',b',a') = tColor
    fontErr =  unsafePerformIO $! loadFontFile fontpath 
    fontpath
      | gamefont == MainFont = "Fonts/ProggyTinySZ.ttf"
      | otherwise = "Fonts/neoletters.ttf"



displayChunks :: [String] -> GameFont -> Float -> (Word8,Word8,Word8,Word8) -> (Word8,Word8,Word8,Word8) -> Picture
displayChunks [] gamefont size bColor tColor  = blank
displayChunks (x:xs) gamefont size bColor tColor = displayChunks' (x:xs) gamefont size bColor tColor 0
  where
  displayChunks' :: [String] -> GameFont -> Float -> (Word8,Word8,Word8,Word8) -> (Word8,Word8,Word8,Word8) -> Float -> Picture
  displayChunks' [] gamefont size bColor tColor counter = blank
  displayChunks' (x:xs) gamefont size bColor tColor counter = pictures [( displayChunk x gamefont size bColor tColor ), (translate (0-counter*size*10) (0-size) $ displayChunks' xs gamefont size bColor tColor 0)]





cutText :: Int -> String -> [String]
cutText max text = buildChunks max words [] []
  where
    words = (splitOn " " text)
    buildChunks :: Int -> [String] -> String -> [String] -> [String]
    buildChunks max [] rest chunks = chunks ++ [rest] 
    buildChunks max (x:xs) rest chunks
      | 1+ (length rest) + (length x) <= max = if rest == [] then buildChunks max (xs) x chunks else  buildChunks max xs (rest ++ " " ++ x) chunks
      | otherwise = buildChunks max (x:xs) [] (chunks ++ [rest]) 


