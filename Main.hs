module Main where

import Types
import Tools
import Input
import Actors
import Preload  
import Render
import LoadExternal
import LevelHandling
import TextRendering

import Debug.Trace
import Linear.V2
import Sound.ALUT
import System.Exit (exitSuccess)
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import qualified Data.HashMap.Lazy as DHL
import qualified Data.Map.Lazy as M
import Control.Lens


gameName = "Fighting Spirit"
gameVersion = "0.0.1"
windowDimensions = initialState^.config.dimensions
windowOffset = (50,50)

testText = "This is a very good book. You should read it until the end. It would do you good. Do it, come on. Here's how it goes. Once, Taus was very bored. So he decided to animate one of his creations , a doll made of prime earth with his breath. By writing on the doll, as if writing a spell, he realized he could control how the doll could act, to an extent. That's how the first Golem was born."
currentItemName = "The Golemic Mystery , Volume 1, By Jarzeno Jarzenoer"


prelText =  PreloadedText 
  { _introMessage = renderIntro "Enter player name" MainFont 40 (setAlpha tWhite 0 ) tWhite
  , _loading       = renderDescription "Loading..." OtherFont 40 (setAlpha tWhite 0) tWhite
  , _currentDesc  = blank}


initialState = Game
  { _status     = Status { _stage = 0, _battleStatus = baseBattle, _scene = Introduction, _cLevel = emptyLevel, _cMap = blank, _camera = V2 0 0, _cameraDirection = V2 0 0, _pInput = "" }
  , _events = []
  , _preloadedText = prelText
  , _player = Player { _party = M.fromList [(0,fighter) ,(1,archer)]}
  ,_gText =  GameText { _currentItem = (testText, currentItemName), _dialog = "", _combat = "", _input = "" }
  , _flags      = Flags { _exit = False, _itemDesc = False, _initialized = False }
  , _config = Config { _dimensions = (1024,768), _tilesize = (23, 23) }
  }   
    

window :: Display
window = InWindow (gameName ++ gameVersion) windowDimensions windowOffset

background :: Color
background = black

main :: IO ()
main = do 
          playIO window background 60 initialState renderIO handleInputIO updateGameIO

--using IO ops : quitting the game, saving, loading, playing music.
updateGameIO :: Float -> Game -> IO Game
updateGameIO dt game
  | game^.flags.exit == True = trace "Quitting Game..." exitSuccess
  | otherwise = return(updateGame dt game)


--pure game logic, and reading unchanging files (levels, external config)
updateGame :: Float -> Game -> Game
updateGame dt game
  | game^.events == [] = handleScene game
  | otherwise = handleEvents game
  where
    (x:xs) = game^.events
    handleEvents game = case x of
      NewLevel a -> trace ("loading new level : " ++ a ) $ clean $ loadLevel $ game & status.cLevel .~ (getLevel a)
      NewScene a -> trace ("switching to scene : " ++ (show a)) $ clean $ game & status.scene .~ a
    clean game = game & events .~ xs
      where 
        (x:xs) = game^.events
    handleScene game
      | game^.status.scene == Introduction = game
      | game^.status.scene == InGame = updateCamera game



