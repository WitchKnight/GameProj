module TurnSystem where

import Tools
import Types

import Control.Lens
import Data.HashMap.Lazy as DHL
--returns game with Battlestatus updated with the actor currently taking his turn

newTurn :: Game -> Game
newTurn game = game & status.battleStatus %~ changeTurn elect
  where
    actors' = game^.status.cLevel.actors
    --check for actor with highest init
    
    applyFilter n mapactor
      | isSingleton $ filter' n mapactor = 
      | otherwise = applyFilter (n+1) mapactor
      where
        filter' n hmap
          | n == 1 = DHL.foldl' 
           --if more than one with same init, check for one with higher luck
          | n == 2 = 
           --if more than one with same luck, check for one with lower level
          | n == 3 = 
           --if more than one with same level, check for one with lower stat total
          | n == 4 =
           --if more than one with same lst, take the one with first ID
              
        

        
        
       


















--  ~ data ActorStats = ActorStats
  --  ~ { _actionStats  :: ActionStats
  --  ~ , _expStats     :: ExpStats
  --  ~ , _SpiritStats  :: SpiritStats
  --  ~ , _BodyStats    :: BodyStats
  --  ~ , _mastery      :: Mastery
  --  ~ } deriving (Show,Eq)
  
--  ~ data ActionStats = ActionStats
  --  ~ { _fatigue    :: V2 Int
  --  ~ , _initiative :: V2 Int
  --  ~ , _reflexes   :: Int
  --  ~ , _actState   :: (ActionState, Int)
  --  ~ } deriving (Show,Eq)

--  ~ data ActionState = Ready | Waiting deriving (Show,Eq,Read)

--  ~ data ExpStats = ExpStats
  --  ~ { _actorLevel  :: Int
  --  ~ , _experience   :: V2 Int
  --  ~ } deriving (Show,Eq)

--  ~ data BodyStats  = BodyStats
  --  ~ { _constitution :: Int
  --  ~ , _strength     :: Int
  --  ~ , _tolerance    :: Int
  --  ~ , _agility      :: Int
  --  ~ , _precision    :: Int
  --  ~ } deriving (Show,Eq)

--  ~ data SpiritStats = SpiritStats 
  --  ~ { _power        :: Int
  --  ~ , _passion      :: Int
  --  ~ , _grounding    :: Int
  --  ~ , _awareness    :: Int
  --  ~ , _insight      :: Int
  --  ~ , _resilience   :: Int
  --  ~ , _luck         :: Int
  --  ~ } deriving (Show,Eq)
