module Input where

import Types
import Tools
import TextRendering
import Render

import Debug.Trace
import Control.Lens
import Linear.V2
import Graphics.Gloss.Interface.IO.Game

import GHC.Word

mil = 20
gameEsc :: Game -> IO Game
gameEsc game= do 
                    --  ~ playSoundEffect Cancel
                    return newgame
                    where
                      newgame
                        | game^.status.scene ==  Introduction = gameExit game
                        | game^.status.scene ==  InGame       = queueEvents [(NewScene Menu)] game
                        | game^.status.scene ==  Loading      = game
                        | game^.status.scene ==  Menu         = gameExit game

gameConfirm :: Game -> IO Game
gameConfirm game = do 
                    --  ~ playSoundEffect Confirm
                    return newgame
                    where
                      newgame
                        | game^.status.scene ==  Introduction = queueEvents [(NewScene Loading),(NewLevel "Testlevel")] game
                        | game^.status.scene ==  InGame       = game
                        | game^.status.scene ==  Loading      = game
                        | game^.status.scene ==  Menu         = queueEvents [(NewScene InGame)] game

handleInputIO :: Event -> Game -> IO Game
handleInputIO event game =  inputSelect --trace (show event) $ : debug
  where 
    inputSelect
      | game^.status.scene ==  Introduction = handleIntro event game
      | game^.status.scene ==  InGame       = handleInGame event game
      | game^.status.scene ==  Loading      = handleLoading event game
      | game^.status.scene ==  Menu         = handleMenu event game
      | otherwise = return $ game 

handleIntro (EventKey (Char x ) Down _ _) game 
  | x /= '\b' = return $ game & status.pInput %~ (addchar x)
  | otherwise = trace "bcsp" $ return $ game & status.pInput .~ (removechar)
    where
      (x':xs') = reverse $ game^.status.pInput
      addchar x towhat
        | length (towhat) < mil = towhat ++ [x]
        | otherwise = towhat
      removechar
        | length (game^.status.pInput) > 0 = reverse xs'
        | otherwise = game^.status.pInput
    
handleIntro (EventKey (SpecialKey x ) Down _ _) game         
  | x == KeyEsc       = gameEsc game
  | x == KeyEnter     = gameConfirm game
  | x == KeySpace     = return $ game & status.pInput .~ (addspace)
  | x == KeyLeft || x == KeyBackspace = trace "bcsp" $ return $ game & status.pInput .~ (removechar)
  | otherwise =  return game
    where
      (x':xs') = reverse $ game^.status.pInput
      addspace 
        | length (x':xs') < mil = reverse (' ':x':xs')
        | otherwise = reverse (x':xs')
      removechar
        | length (game^.status.pInput) > 0 = reverse xs'
        | otherwise = game^.status.pInput
handleIntro _ game = return game

handleInGame (EventKey (Char x ) Down _ _) game 
  | x == 't' = do
                --  ~ playSoundEffect Step
                return game
  | x == 'd' = return $ game  & flags.itemDesc %~ not
                              & preloadedText.currentDesc %~ trigPic (renderDescription (game^.gText.currentItem._1) MainFont 30 (setAlpha tWhite 0 ) tWhite)

handleInGame (EventKey (SpecialKey x ) Down _ _) game 
  | x `elem` [KeyDown,KeyUp,KeyLeft,KeyRight] = return $ game & status.cameraDirection %~ (+ chosenDir)
  | x == KeyEsc   = gameEsc game
  | x == KeyEnter = gameConfirm game 
  | otherwise = return game
  where
    chosenDir
      | x == KeyDown  = V2 0 1
      | x == KeyUp    = V2 0 (-1)
      | x == KeyLeft  = V2 1 0
      | x == KeyRight = V2 (-1) 0

handleInGame (EventKey (SpecialKey x ) Up _ _) game
  | x `elem` [KeyDown,KeyUp,KeyLeft,KeyRight] = return $ game & status.cameraDirection %~ (+ (-chosenDir))
  | otherwise = return game 
  where
    chosenDir
      | x == KeyDown  = V2 0 1
      | x == KeyUp    = V2 0 (-1)
      | x == KeyLeft  = V2 1 0
      | x == KeyRight = V2 (-1) 0
handleInGame _ game = return game 

handleLoading _ game = return game

handleMenu (EventKey (SpecialKey x ) Down _ _) game         
  | x == KeyEsc   = gameEsc game
  | x == KeyEnter = gameConfirm game  
  | otherwise = return game

handleMenu _ game = return game
  



