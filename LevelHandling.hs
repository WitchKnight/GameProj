module LevelHandling where

import Types 
import Tools

import qualified Data.HashMap.Lazy as DHL
import qualified Data.Map.Lazy as M
import Control.Lens
import Linear.V2

import Debug.Trace

loadLevel :: Game -> Game
loadLevel game = newgame''
  where
    newgame  = placeActors game
    newgame' = trace "Actors placed" $ makeLevelPicture newgame
    newgame'' = trace "Level image loaded" $ queueEvents [(NewScene InGame)] newgame'
     
placeActors :: Game -> Game
placeActors game = game & status.cLevel.actors %~ (addParty)
  where
    addParty withwho = M.union withwho actors'
    actors' = M.mapWithKey setFormation $ M.map (setCoordinates) $ game^.player.party
    setFormation n act = act & acoord %~ (+ V2 n 0)
    setCoordinates act = act & acoord .~ (topleftSquare) 
    V2 (topleftSquare) (rightbottomSquare) = game^.status.cLevel.startingSquare 
