module Actors where

import Types
import Data.HashMap.Lazy

import Linear.V2

fighter = Actor 
  { _name             = "testfighter"
  , _acoord           = V2 0 0 
  , _aclass           = fighterClass
  , _astats           = baseStats
  --  ~ , _inventory        :: Inventory
  --  ~ , _abilities        :: Abilities
  --  ~ , _statusEffects    :: StarusEffects
  }
  
  
fighterClass = ActorClass
  { _vocations = fromList [(0,Fencer)]
  , _actorsprite = Swordman
  }

archer = Actor 
  { _name             = "testarcher"
  , _acoord           = V2 0 0 
  , _aclass           = archerClass
  , _astats           = baseStats
  --  ~ , _inventory        :: Inventory
  --  ~ , _abilities        :: Abilities
  --  ~ , _statusEffects    :: StatusEffects
  }
  
  
archerClass = ActorClass
  { _vocations = fromList [(0,Archer)]
  , _actorsprite = Bowman
  }


salamander = Actor
  { _name         = "salamander"
  , _acoord       = V2 0 0
  , _aclass       = salamanderClass
  , _astats           = baseStats
  }

salamanderClass = ActorClass
  { _vocations = empty
  , _actorsprite = Salamander
  }

loadMapActor :: Int -> Actor
loadMapActor n
  | n == 1 = salamander


baseAction = ActionStats
  { _fatigue = V2 0 0
  , _initiative = V2 0 0
  , _reflexes = 0
  , _actState = (Waiting, 0)
  }
  
baseExp = ExpStats
  { _actorLevel = 0
  , _experience  = V2 0 0
  }

baseBody = BodyStats
  { _constitution = 1
  , _strength     = 1
  , _tolerance    = 1
  , _agility      = 1
  , _precision    = 1
  }



baseSpirit = SpiritStats
  { _power        = 1 
  , _passion      = 1
  , _grounding    = 1
  , _awareness    = 1
  , _insight      = 1
  , _resilience   = 1
  , _luck         = 1
  }

baseStats = ActorStats
  { _actionStats = baseAction
  , _expStats    = baseExp
  , _SpiritStats = baseSpirit
  , _BodyStats   = baseBody
  }     








--  ~ data ActorStats = ActorStats
  --  ~ { _actionStats  :: ActionStats
  --  ~ , _expStats     :: ExpStats
  --  ~ , _SpiritStats  :: SpiritStats
  --  ~ , _BodyStats    :: BodyStats
  --  ~ , _mastery      :: Mastery
  --  ~ } deriving (Show,Eq)
  
--  ~ data ActionStats = ActionStats
  --  ~ { _fatigue    :: V2 Int
  --  ~ , _initiative :: V2 Int
  --  ~ , _reflexes   :: Int
  --  ~ , _actState   :: (ActionState, Int)
  --  ~ } deriving (Show,Eq)

--  ~ data ActionState = Ready | Waiting deriving (Show,Eq,Read)

--  ~ data ExpStats = ExpStats
  --  ~ { _actorLevel  :: Int
  --  ~ , _experience   :: V2 Int
  --  ~ } deriving (Show,Eq)

--  ~ data BodyStats  = BodyStats
  --  ~ { _constitution :: Int
  --  ~ , _strength     :: Int
  --  ~ , _tolerance    :: Int
  --  ~ , _agility      :: Int
  --  ~ , _precision    :: Int
  --  ~ } deriving (Show,Eq)

--  ~ data SpiritStats = SpiritStats 
  --  ~ { _power        :: Int
  --  ~ , _passion      :: Int
  --  ~ , _grounding    :: Int
  --  ~ , _awareness    :: Int
  --  ~ , _insight      :: Int
  --  ~ , _resilience   :: Int
  --  ~ , _luck         :: Int
  --  ~ } deriving (Show,Eq)
--  ~ -- MAP TYPES
